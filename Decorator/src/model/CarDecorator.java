/* Il pattern Decorator è utilizzato per modificare le funzionalità di un oggetto a runtime. Si utilizza
 * l'ereditarietà o la composizione per estendere i comportamenti di un oggetto ma questo avviene a 
 * compiletime senza possibilità di intervenire a runtime. Nell'esempio si ha un'interfaccia Car che viene
 * implementata dalla classe BasicCar che a sua volta è estesa da SportsCar e LuxuryCar. Supponiamo che a 
 * runtime vogliamo aggiungere a una tipologia di macchina le caratteristiche di un'altra, la situazione è 
 * complessa e lo diventa ancora di più se vogliamo far ciò per più macchine. 
 * Sono necessari i seguenti componenti per implementare il patter:
 * - COMPONENT INTERFACE: (Car nell'esempio) L'interfaccia o la classe astratta che definisce i metodi che 
 * vogliamo implementare
 * - COMPONENT IMPLEMENTATION: (BasicCar nell'esempio) L'implementazione base della component interface
 * - DECORATOR: (CarDecorator nell'esempio) Implementa la component interface ed ha una relazione HAS-A
 * con essa. L'oggetto contenuto deve essere accessibile direttamente dalle sotto classi per cui si dichiara
 * protected.
 * - CONCRETE DECORATORS: (SportsCar e LuxuryCar nell'esempio) estendono il decorator e modificano i 
 * comportamenti 
 */
package model;

public class CarDecorator implements Car {

	protected Car car;
	
	public CarDecorator(Car c){
		this.car=c;
	}
	
	@Override
	public void assemble() {
		this.car.assemble();
	}

}