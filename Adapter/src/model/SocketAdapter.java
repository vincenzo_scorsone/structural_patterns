/* L'esempio degli adattatori per le prese è lo stesso concetto applicato al pattern Adapter. 
 * Permette a due interfacce non collegate di lavorare insieme attraverso ad un oggetto Adapter*/

package model;

//L'adattatore che produce voltaggi diversi
public interface SocketAdapter {
	
	public Volt get120Volt();
	
	public Volt get12Volt();
	
	public Volt get3Volt();
	
}
