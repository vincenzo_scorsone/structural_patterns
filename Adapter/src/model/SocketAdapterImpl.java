package model;


//Nell'implementazione ho utilizzato l'ereditarietà ma si poteva pure usare la composizione 
// mettendo un oggetto Socket come attributo della classe
public class SocketAdapterImpl extends Socket implements SocketAdapter{
	
	private Volt convertVolt(Volt v, int i) {
		return new Volt(v.getVolts()/i);
	}
	
	@Override
	public Volt get120Volt() {
		return getVolt();
	}

	@Override
	public Volt get12Volt() {
		return convertVolt(getVolt(), 10);
	}

	@Override
	public Volt get3Volt() {
		return convertVolt(getVolt(), 40);
	}

}
