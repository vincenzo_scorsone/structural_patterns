package model;

//Classe che rappresenta una presa a muro
public class Socket {
	
	//rapprensenta un voltaggio constante (120 prese Americane)
	public Volt getVolt(){
		return new Volt(120);
	}
	
}
