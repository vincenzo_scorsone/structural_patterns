/* Facade fornisce un'interfaccia per unificare un insieme di interfacce e renderne più facile l'utilizzo.
 * E' utile perchè se un client deve utilizzare un sistema con un gran numero di interfacce e/o queste 
 * interfacce hanno dei nomi confusionari, Facade semplifica notevolmente l'utilizzo.
 * 
 * Altri dettagli:
 * - Facade è un aiuto per il client ma non nasconde le varie interfacce ad esso.
 * - Facade può essere utilizzato in ogni momento del development, di solito succede al crescere del numero 
 * delle interfacce e della complessità del sistema.
 * - Le sotto-interfacce non sono a conoscenza della Facade.
 * - Deve essere applicato  a delle interfacce che svolgono dei compiti simili (nell'esempio entrambe si 
 * collegano ad un database e generano report)
 * - Si può integrare con il Factory.
 * 
 * */
 
package model;

import java.sql.Connection;

public class HelperFacade {

	public static void generateReport(DBTypes dbType, ReportTypes reportType, String tableName){
		Connection con = null;
		switch (dbType){
		case MYSQL: 
			con = MySqlHelper.getMySqlDBConnection();
			MySqlHelper mySqlHelper = new MySqlHelper();
			switch(reportType){
			case HTML:
				mySqlHelper.generateMySqlHTMLReport(tableName, con);
				break;
			case PDF:
				mySqlHelper.generateMySqlPDFReport(tableName, con);
				break;
			}
			break;
		case ORACLE: 
			con = OracleHelper.getOracleDBConnection();
			OracleHelper oracleHelper = new OracleHelper();
			switch(reportType){
			case HTML:
				oracleHelper.generateOracleHTMLReport(tableName, con);
				break;
			case PDF:
				oracleHelper.generateOraclePDFReport(tableName, con);
				break;
			}
			break;
		}
		
	}
	
	public static enum DBTypes{
		MYSQL,ORACLE;
	}
	
	public static enum ReportTypes{
		HTML,PDF;
	}
}
