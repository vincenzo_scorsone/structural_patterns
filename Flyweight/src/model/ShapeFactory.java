/* Flyweight è un pattern usato quando bisogna creare tanti oggetti di una classe. Flyweight può 
 * essere applicato per ridurre il carico in memoria attraverso la condivisione di oggetti. 
 * Prima di applicarlo bisogna tener conto dei seguenti fattori:
 * - Il numero di oggetti che si devono creare
 * - La creazione è pesante e richiede tempo
 * - Le proprietà di un oggetto possono essere divisein intrinseche ed estrinseche, quest'ultime sono
 * quelle definite dal client(per esempio in questo codice possono essere la larghezza e il colore)
 * 
 * Per applicare il pattern bisogna creare una Factory che sarà usata dal client per istanziare oggetti.
 * Conterrà una Map che non sarà accessibile dal client. Ogni volta che il client richiede un'istanza 
 * dell'oggetto questa viene presa dall'hashMaP, se non esiste viene creata, messa nella mappa e ritornata.
 * Dobbiamo assicurarci che le proprietà intriseche vengano definite al momento della creazione.
 * 
 * Altre informazioni:
 * -Il pattern aumenta la complessità e se il numero di oggetti da creare è grande c'è spreco di tempo e
 * memoria. Bisogna utilizzarlo razionalmente in base alle esigenze.
 * -Se il numero delle proprietà intriseche non conviene utilizzarlo perchè aumenta la complessità
 * della Factory.
 * 
 * */



package model;

import java.util.HashMap;

public class ShapeFactory {

	private static final HashMap<ShapeType, Shape> shapes = new HashMap<ShapeType, Shape>();

	public static Shape getShape(ShapeType type) {
		Shape shapeImpl = shapes.get(type);

		if (shapeImpl == null) {
			if (type.equals(ShapeType.OVAL_FILL)) {
				shapeImpl = new Oval(true);
			} else if (type.equals(ShapeType.OVAL_NOFILL)) {
				shapeImpl = new Oval(false);
			} else if (type.equals(ShapeType.LINE)) {
				shapeImpl = new Line();
			}
			shapes.put(type, shapeImpl);
		}
		return shapeImpl;
	}

	public static enum ShapeType {
		OVAL_FILL, OVAL_NOFILL, LINE;
	}
}