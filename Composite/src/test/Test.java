package test;

import model.Circle;
import model.Drawing;
import model.Shape;
import model.Triangle;

public class Test {

	public static void main(String[] args) {
		Shape sh1= new Triangle();
		Shape sh2= new Triangle();
		Shape sh3= new Circle();
		
		Drawing drawing= new Drawing();
		drawing.add(sh1);
		drawing.add(sh2);
		drawing.add(sh3);
		
		drawing.draw("red");
		
		drawing.clear();
		
		
		drawing.add(sh1);
		drawing.add(sh1);
		
		drawing.draw("yellow");;
		

	}

}
