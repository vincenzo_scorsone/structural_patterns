package model;

public abstract class Shape {
	public abstract void draw(String fillColor);
}
