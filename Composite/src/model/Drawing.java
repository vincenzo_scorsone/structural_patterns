/* Composite è applicato quando dobbiamo creare una struttura per gestire oggetti nello stesso modo.
 * Consiste nei seguenti componenti:
 * - COMPONENTE BASE: E' l'interfaccia o la classe astratta di base per la composizione di tutti 
 * gli oggetti, è quella che viene utilizzata per lavorare con questi oggetti. (In questo esempio
 * è Shape)
 * - LEAF: Definisce i comportamenti per l'oggetti della composizione, è il blocco costruito per
 * la composizione e implementa il componente base. (In questo esempio è costituito da Triangle e Circle)
 * - COMPOSITE: Consiste nella classe che realizza effettivamente il pattern (Drawing nell'esempio).
 * Contiene una struttura di istanze delle classi del blocco LEAF, e può contenere vari metodi come
 * aggiungi, cancella, ricerca ecc.
 * 
 * Ulteriori info:
 * - Può essere applicato solo ad una gerarchia di oggetti, utilizzando il riferimento della classe
 * che sta più in alto nella gerarchia
 * - Si può creare una struttura ad albero
 * */
 
package model;

import java.util.ArrayList;
import java.util.List;

public class Drawing extends Shape {
	
	private List<Shape> list= new ArrayList<Shape>();

	@Override
	public void draw(String fillColor) {
		for(Shape sh: list)
			sh.draw(fillColor);
		
	}
	
	public void add(Shape s){
		this.list.add(s);
	}
	
	public void remove(Shape s){
		this.list.remove(s);
	}
	
	public void clear(){
		this.list.clear();
	}
	

}
