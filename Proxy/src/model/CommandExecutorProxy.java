/* Il pattern Proxy è utilizzato per controllare gli accessi ad una determinata funzionalità. Un 
 * esempio logico a cui può essere applicato è quello di una classe che esegue comandi sul sistema.
 * Se un client utilizza questa classe può essere pericoloso. Il pattern può venirci in aiuto. In questo
 * esempio viene simulato che solo l'admin possa lanciare un comando.
 */
package model;

public class CommandExecutorProxy implements CommandExecutor {

	private boolean isAdmin;
	private CommandExecutor executor;
	
	public CommandExecutorProxy(String user, String pwd){
		if("Vinc".equals(user) && "scr".equals(pwd)) isAdmin=true;
		executor = new CommandExecutorImpl();
	}
	
	@Override
	public void runCommand(String cmd) throws Exception {
		if(isAdmin){
			executor.runCommand(cmd);
		}else{
			if(cmd.trim().startsWith("rm")){
				throw new Exception("rm command is not allowed for non-admin users.");
			}else{
				executor.runCommand(cmd);
			}
		}
	}

}

