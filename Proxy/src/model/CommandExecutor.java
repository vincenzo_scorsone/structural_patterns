package model;

public interface CommandExecutor {
	public void runCommand(String cmd) throws Exception;
}
