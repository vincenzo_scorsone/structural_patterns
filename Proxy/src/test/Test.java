package test;

import model.CommandExecutor;
import model.CommandExecutorProxy;

public class Test {

	public static void main(String[] args) {
			CommandExecutor executor = new CommandExecutorProxy("Pankaj", "wrong_pwd");
			try {
				executor.runCommand("ls -ltr");
				executor.runCommand(" rm -rf abc.pdf");
			} catch (Exception e) {
				System.out.println("Exception Message::"+e.getMessage());
			}
			
			CommandExecutor executor1 = new CommandExecutorProxy("Vinc", "scr");
			try {
				executor1.runCommand("ls -ltr");
				executor1.runCommand(" rm -rf abc.pdf");
			} catch (Exception e) {
				System.out.println("Exception Message::"+e.getMessage());
			}
	}

}
