package test;

import model.GreenColor;
import model.Pentagon;
import model.RedColor;
import model.Shape;
import model.Triangle;

public class Test {

	public static void main(String[] args) {
		Shape tri = new Triangle(new RedColor());
		tri.applyColor();
		
		Shape pent = new Pentagon(new GreenColor());
		pent.applyColor();

	}

}
