/* Il pattern astrae disaccoppiare l'astrazione dall'implementazione. L'implementazione del pattern favorisce la composizione rispetto 
 * all'ereditarietà. Nell'esempio il bridge viene applicato tra Shape e color con la composizione.
 * 
 */
package model;

public interface Color {
	public void applyColor();
}
